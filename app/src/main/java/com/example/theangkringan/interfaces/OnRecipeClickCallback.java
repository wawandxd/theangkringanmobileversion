package com.example.theangkringan.interfaces;

import com.example.theangkringan.models.RecipeModel;

public interface OnRecipeClickCallback {
    void onItemClicked(RecipeModel data);
}
