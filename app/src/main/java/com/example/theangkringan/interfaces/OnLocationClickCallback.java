package com.example.theangkringan.interfaces;

import com.example.theangkringan.models.LocationModel;

public interface OnLocationClickCallback {
    void onItemClicked(LocationModel data);
}
